# Vagrant Zabbix
Intuito desde repositorio é meu apredizado sobre IaC. Nele usei o evento feito pela equipe do Jorge Pretel e pelo Rogerio na Jornada de Monitoramento.

Aqui desenvolvi um script para criar uma instancia no VirtualBox atraves do Vagrant para subir um Zabbix Server.

Na pasta Provision tem um script chamado server.sh e é nele que acontece toda a magia... 

Vários testes foram feitos diretamente no shell como por exemplo o corte de variáveis com é no caso da variável VERSAO_ZABBIX_SERVER_MYSQL=$(yum info zabbix-server-mysql | grep -e Version | awk'{print $3}').


Alguns links que ajudarão a entender melhor este projeto.

Usei o Vagrant - o utilitário de linha de comando para gerenciar o ciclo de vida das máquinas virtuais.
https://www.vagrantup.com/docs


Mostra como usar a poção provision no vagrant para rodar instruções na instancia 
https://www.vagrantup.com/docs/provisioning/basic_usage


#### Vou atualizando...

Se quiser trocar ideias sobre Tecnologia, meu [linkedin](https://www.linkedin.com/in/soaresnetoh/)
