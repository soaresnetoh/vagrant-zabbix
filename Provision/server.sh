#!/bin/bash

$Variaveis
IP_SERVER=$(ip a | grep eth2 | grep inet | awk '{print $2}' | rev | cut -c4- | rev)
SENHA="Jornada@2021!"
HOSTNAME="centos7.hsninformatica.com.br"
########################################
# Alterando a senha do usuario root
sudo usermod -p $(openssl passwd -1 $SENHA) root

########################################
# Acertando o timezone da maquina
sudo timedatectl set-timezone America/Sao_Paulo

########################################
# Instalando alguns utilitários
sudo yum install -y net-tools.x86_64 vim nano epel-release.noarch wget curl tcpdump.x86_64 

########################################
# Desabilitar o selinux (ferramenta de segurança do centOs)
sudo sed -i s/"SELINUX=enforcing"/"SELINUX=disable"/g  /etc/selinux/config
sudo setenforce 0

########################################
# Instalar o repositorio do MySql no CentOs 7
sudo rpm -ivh http://repo.mysql.com/mysql80-community-release-el7.rpm
sudo yum install -y mysql-community-server

########################################
# Habilitar o mysql para iniciar automaticamente com o servidor
sudo systemctl enable --now mysqld
sudo systemctl status mysqld

########################################
# Pegar a senha padrão do mysql
SENHA_MYSQL=$(sudo grep 'temporary password' /var/log/mysqld.log | awk '{print $13}')
COMANDO="ALTER USER 'root'@'localhost' IDENTIFIED BY '"$SENHA"'"
echo $SENHA_MYSQL

########################################
# Trocar Senha do MySql - Atualizar MySql

sudo MYSQL_PWD=$SENHA_MYSQL mysql -u root -e "$COMANDO"  --connect-expired-password
sudo MYSQL_PWD=$SENHA mysql -u root -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
sudo MYSQL_PWD=$SENHA mysql -u root -e "DELETE FROM mysql.user WHERE User=''"
sudo MYSQL_PWD=$SENHA mysql -u root -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
sudo MYSQL_PWD=$SENHA mysql -u root -e "FLUSH PRIVILEGES"

########################################
# Criar o base e usuario zabbix e setar privilegios
sudo MYSQL_PWD=$SENHA mysql -u root -p"$SENHA" -e "CREATE DATABASE zabbix character set utf8 collate utf8_bin"
sudo MYSQL_PWD=$SENHA mysql -u root -p"$SENHA" -e "CREATE USER zabbix@localhost identified with mysql_native_password by '$SENHA'"
sudo MYSQL_PWD=$SENHA mysql -u root -p"$SENHA" -e "GRANT ALL PRIVILEGES on zabbix.* to 'zabbix'@'localhost'"
sudo MYSQL_PWD=$SENHA mysql -u root -p"$SENHA" -e "FLUSH PRIVILEGES"

########################################
# Instalar zabbix server https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-server-mysql-5.0.11-1.el7.x86_64.rpm
# aqui setei a versão do zabbix-server-mysql para 
sudo rpm -ivh http://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
sudo yum info zabbix-server-mysql-5.0.11-1.el7.x86_64
sudo yum install -y zabbix-server-mysql-5.0.11-1.el7.x86_64
VERSAO_ZABBIX_SERVER_MYSQL=$(yum info zabbix-server-mysql | grep -e Version | awk '{print $3}')

########################################
# Dando carga no mysql --- verificar qual versão do zabbix foi instalada para o zcat correto
#sudo zcat /usr/share/doc/zabbix-server-mysql-5.0.11/create.sql.gz | sudo MYSQL_PWD=$SENHA mysql -u zabbix -p zabbix
sudo zcat /usr/share/doc/zabbix-server-mysql-$VERSAO_ZABBIX_SERVER_MYSQL/create.sql.gz | sudo MYSQL_PWD=$SENHA mysql -u zabbix zabbix

########################################
# Configurar banco de dados no zabbix
sudo sed -i s/"DBPassword= "/"DBPassword="$SENHA/g  /etc/zabbix/zabbix_server.conf

########################################
# Habilitar o zabbix server para iniciar junto com o servidor
sudo systemctl enable --now zabbix-server.service 
sudo systemctl status zabbix-server.service

########################################
# Instalação do FrontEnd
# Precisa habilitar o RedHat Soft Colections
sudo yum install -y centos-release-scl
sudo sed -i '11s/enabled=0/enabled=1/' /etc/yum.repos.d/zabbix.repo 

########################################
# Instalar pacotes (se fosse centOs8 nao precisava do -scl)
sudo yum install -y zabbix-web-mysql-scl zabbix-apache-conf-scl

########################################
# Configurando PHP
sudo sed -i "s/; php_value\[date.timezone\]\ \=\ Europe\/Riga/php_value\[date.timezone\]\ \=\ America\/Sao_Paulo/g" /etc/opt/rh/rh-php72/php-fpm.d/zabbix.conf

########################################
# Habilitar para iniciar junto com o servidor
sudo systemctl enable --now httpd rh-php72-php-fpm.service 
sudo systemctl status httpd rh-php72-php-fpm.service 

########################################
### Firewall
# Criar regras no Firewalld
sudo systemctl status firewalld

########################################
# Caso o firewall esteja desabilitado, usuario
sudo systemctl enable firewalld.service 
sudo systemctl start firewalld
sudo systemctl status firewalld.service 

########################################
# Liberar porta de acesso externo
sudo firewall-cmd --permanent --add-port=80/tcp

########################################
# Recarregar Regras
sudo firewall-cmd --reload


########################################################
# ACESSAR O NAVEGADOR COM O http://<ipdoserver>/zabbix
# Senha do Banco (Jornada@2021!)
# senha padrão do zabbix: Admin/zabbix
########################################################


########################################
# Instalando e configurando o agente

# Instalar o agente no CentOs 7 - Zabbix Agent
sudo yum install -y zabbix-agent

########################################
# Editar o arquivo do agente do zabbix
sudo sed -i '66c\Server=$IP_SERVER'  /etc/zabbix/zabbix_agentd.conf
sudo sed -i '107c\ServerActive=$IP_SERVER'  /etc/zabbix/zabbix_agentd.conf
sudo sed -i '118c\Hostname=$HOSTNAME'  /etc/zabbix/zabbix_agentd.conf

########################################
# Adicionar regras no firewall
sudo firewall-cmd --add-port=10050/tcp --permanent 
sudo firewall-cmd --reload


########################################
# Habilitar para iniciar junto com o servidor
sudo systemctl start zabbix-agent
sudo systemctl enable --now zabbix-agent
sudo systemctl status zabbix-agent

########################################
#### FIM S